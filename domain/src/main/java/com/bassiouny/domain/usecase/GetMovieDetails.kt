package com.bassiouny.domain.usecase

import com.bassiouny.domain.entities.MovieEntity
import com.bassiouny.domain.repository.MovieRepository
import com.bassiouny.domain.util.Result

/**
 * Created by Bassiouny
 **/
class GetMovieDetails(
    private val movieRepository: MovieRepository
) {
    suspend fun getMovie(movieId: Int): Result<MovieEntity> = movieRepository.getMovie(movieId)
}
