package com.bassiouny.domain.usecase

import com.bassiouny.domain.repository.MovieRepository
import com.bassiouny.domain.util.Result

/**
 * @author by Bassiouny
 */
class CheckFavoriteStatus(
    private val movieRepository: MovieRepository
) {
    suspend fun check(movieId: Int): Result<Boolean> = movieRepository.checkFavoriteStatus(movieId)
}
