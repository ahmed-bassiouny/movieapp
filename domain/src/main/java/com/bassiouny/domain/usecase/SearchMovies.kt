package com.bassiouny.domain.usecase

import com.bassiouny.domain.entities.MovieEntity
import com.bassiouny.domain.repository.MovieRepository
import com.bassiouny.domain.util.Result

/**
 * @author by Bassiouny
 */
class SearchMovies(
    private val movieRepository: MovieRepository
) {
    suspend fun search(query: String): Result<List<MovieEntity>> = movieRepository.search(query)
}
