package com.bassiouny.domain.usecase

import com.bassiouny.domain.repository.MovieRepository

/**
 * @author by Bassiouny
 */
class RemoveMovieFromFavorite(
    private val movieRepository: MovieRepository
) {
    suspend fun remove(movieId: Int) = movieRepository.removeMovieFromFavorite(movieId)
}
