package com.bassiouny.domain.usecase

import com.bassiouny.domain.entities.MovieEntity
import com.bassiouny.domain.repository.MovieRepository
import com.bassiouny.domain.util.Result

/**
 * Created by Bassiouny
 **/
open class GetMovies(
    private val movieRepository: MovieRepository
) {
    suspend fun execute(): Result<List<MovieEntity>> = movieRepository.getMovies()
}
