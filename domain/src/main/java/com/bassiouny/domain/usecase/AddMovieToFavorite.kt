package com.bassiouny.domain.usecase

import com.bassiouny.domain.repository.MovieRepository

/**
 * @author by Bassiouny
 */
class AddMovieToFavorite(
    private val movieRepository: MovieRepository
) {
    suspend fun add(movieId: Int) = movieRepository.addMovieToFavorite(movieId)
}
