package com.bassiouny.domain.usecase

import com.bassiouny.domain.entities.MovieEntity
import com.bassiouny.domain.repository.MovieRepository
import com.bassiouny.domain.util.Result

/**
 * @author by Bassiouny
 */
class GetFavoriteMovies(
    private val movieRepository: MovieRepository
) {
    suspend fun getFavoriteMovies(): Result<List<MovieEntity>> = movieRepository.getFavoriteMovies()
}
