package com.bassiouny.domain.entities

/**
 * Created by Bassiouny
 */
data class MovieEntity(
    val id: Int,
    val title: String,
    val releaseDate: String,
    val image: String,
    val rating:String,
    val originalLanguage:String
)
