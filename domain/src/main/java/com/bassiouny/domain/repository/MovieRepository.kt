package com.bassiouny.domain.repository

import com.bassiouny.domain.entities.MovieEntity
import com.bassiouny.domain.util.Result

/**
 * Created by Bassiouny
 */
interface MovieRepository {
    suspend fun getMovies(): Result<List<MovieEntity>>
    suspend fun search(query: String): Result<List<MovieEntity>>
    suspend fun getMovie(movieId: Int): Result<MovieEntity>
    suspend fun getFavoriteMovies(): Result<List<MovieEntity>>
    suspend fun checkFavoriteStatus(movieId: Int): Result<Boolean>
    suspend fun addMovieToFavorite(movieId: Int)
    suspend fun removeMovieFromFavorite(movieId: Int)
}
