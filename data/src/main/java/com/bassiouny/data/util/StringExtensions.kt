package com.bassiouny.data.util

fun String?.orEmpty():String{
    return this?: ""
}

fun String?.doubleValue():Double{
    return this?.toDoubleOrNull()?: 0.0
}
