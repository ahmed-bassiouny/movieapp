package com.bassiouny.data.util

import kotlinx.coroutines.CoroutineDispatcher

/**
 * Created by Bassiouny
 **/
interface DispatchersProvider {
    fun getIO(): CoroutineDispatcher
    fun getMain(): CoroutineDispatcher
    fun getMainImmediate(): CoroutineDispatcher
    fun getDefault(): CoroutineDispatcher
}
