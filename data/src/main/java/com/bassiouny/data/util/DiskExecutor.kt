package com.bassiouny.data.util

import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Created by Bassiouny
 */
class DiskExecutor : Executor {

    private val diskExecutor: Executor = Executors.newSingleThreadExecutor()

    override fun execute(runnable: Runnable) {
        diskExecutor.execute(runnable)
    }
}
