package com.bassiouny.data.repository.movie

import com.bassiouny.data.api.MovieApi
import com.bassiouny.data.mapper.MovieDataMapper
import com.bassiouny.data.util.DispatchersProvider
import com.bassiouny.domain.entities.MovieEntity
import com.bassiouny.domain.util.Result
import kotlinx.coroutines.withContext

/**
 * Created by Bassiouny
 */
class MovieRemoteDataSource(
    private val movieApi: MovieApi,
    private val dispatchers: DispatchersProvider
) : MovieDataSource.Remote {

    override suspend fun getMovies(): Result<List<MovieEntity>> = withContext(dispatchers.getIO()) {
        return@withContext try {
            val result = movieApi.getMovies("2010","vote_average.desc")
            Result.Success(result.results.map {
                MovieDataMapper.toDomain(it)
            })
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun search(query: String): Result<List<MovieEntity>> = withContext(dispatchers.getIO()) {
        return@withContext try {
            val result = movieApi.search(query)
            Result.Success(result.map {
                MovieDataMapper.toDomain(it)
            })
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}
