package com.bassiouny.data.repository.movie

import android.util.SparseArray
import com.bassiouny.data.exception.DataNotAvailableException
import com.bassiouny.data.util.DiskExecutor
import com.bassiouny.domain.entities.MovieEntity
import com.bassiouny.domain.util.Result
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.withContext

/**
 * Created by Bassiouny
 */
class MovieCacheDataSource(
    private val diskExecutor: DiskExecutor
) : MovieDataSource.Cache {

    private val inMemoryCache = SparseArray<MovieEntity>()

    override suspend fun getMovies(): Result<List<MovieEntity>> = withContext(diskExecutor.asCoroutineDispatcher()) {
        return@withContext if (inMemoryCache.size() > 0) {
            val movies = arrayListOf<MovieEntity>()
            for (i in 0 until inMemoryCache.size()) {
                val key = inMemoryCache.keyAt(i)
                movies.add(inMemoryCache[key])
            }
            Result.Success(movies)
        } else {
            Result.Error(DataNotAvailableException())
        }
    }

    override suspend fun getMovie(movieId: Int): Result<MovieEntity> = withContext(diskExecutor.asCoroutineDispatcher()) {
        val movie = inMemoryCache.get(movieId)
        return@withContext if (movie != null) {
            Result.Success(movie)
        } else {
            Result.Error(DataNotAvailableException())
        }
    }

    override suspend fun saveMovies(movieEntities: List<MovieEntity>) = withContext(diskExecutor.asCoroutineDispatcher()) {
        inMemoryCache.clear()
        for (movie in movieEntities) inMemoryCache.put(movie.id, movie)
    }

    override suspend fun getFavoriteMovies(movieIds: List<Int>): Result<List<MovieEntity>> = withContext(diskExecutor.asCoroutineDispatcher()) {
        return@withContext if (inMemoryCache.size() > 0) {
            val movies = arrayListOf<MovieEntity>()
            movieIds.forEach { id -> movies.add(inMemoryCache.get(id)) }
            Result.Success(movies)
        } else {
            Result.Error(DataNotAvailableException())
        }
    }
}
