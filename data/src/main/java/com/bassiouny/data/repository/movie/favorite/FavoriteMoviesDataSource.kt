package com.bassiouny.data.repository.movie.favorite

import com.bassiouny.data.entities.FavoriteMovieDbData
import com.bassiouny.domain.util.Result

/**
 * @author by Bassiouny
 */
interface FavoriteMoviesDataSource {

    interface Local {
        suspend fun getFavoriteMovieIds(): Result<List<FavoriteMovieDbData>>
        suspend fun addMovieToFavorite(movieId: Int)
        suspend fun removeMovieFromFavorite(movieId: Int)
        suspend fun checkFavoriteStatus(movieId: Int): Result<Boolean>
    }

}
