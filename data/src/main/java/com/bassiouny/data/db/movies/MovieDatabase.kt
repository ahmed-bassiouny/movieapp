package com.bassiouny.data.db.movies

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bassiouny.data.entities.MovieDbData

/**
 * Created by Bassiouny
 */
@Database(
    entities = [MovieDbData::class],
    version = 2,
    exportSchema = false
)
abstract class MovieDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}
