package com.bassiouny.data.db.favoritemovies

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bassiouny.data.entities.FavoriteMovieDbData

/**
 * @author by Bassiouny
 */
@Database(
    entities = [FavoriteMovieDbData::class],
    version = 2,
    exportSchema = false
)
abstract class FavoriteMovieDatabase : RoomDatabase() {
    abstract fun favoriteMovieDao(): FavoriteMovieDao
}
