package com.bassiouny.data.exception

/**
 * Created by Bassiouny
 **/
class DataNotAvailableException : Throwable("Data Not Available")
