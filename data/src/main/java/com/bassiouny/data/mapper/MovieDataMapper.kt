package com.bassiouny.data.mapper

import com.bassiouny.data.entities.MovieData
import com.bassiouny.data.entities.MovieDbData
import com.bassiouny.data.util.orEmpty
import com.bassiouny.domain.entities.MovieEntity

/**
 * Created by Bassiouny
 **/
object MovieDataMapper {

    fun toDomain(movieData: MovieData): MovieEntity = MovieEntity(
        id = movieData.id,
        image = movieData.image.orEmpty(),
        releaseDate = movieData.releaseDate.orEmpty(),
        title = movieData.title.orEmpty(),
        rating = movieData.voteAverage.orEmpty(),
        originalLanguage = movieData.originalLanguage.orEmpty()
    )

    fun toDomain(movieDbData: MovieDbData): MovieEntity = MovieEntity(
        id = movieDbData.id,
        image = movieDbData.image,
        releaseDate = movieDbData.releaseDate,
        title = movieDbData.title,
        rating = movieDbData.rating,
        originalLanguage = movieDbData.originalLanguage.orEmpty()
    )

    fun toDbData(movieEntity: MovieEntity): MovieDbData = MovieDbData(
        id = movieEntity.id,
        image = movieEntity.image,
        releaseDate = movieEntity.releaseDate,
        title = movieEntity.title,
        rating = movieEntity.rating,
        originalLanguage = movieEntity.originalLanguage.orEmpty()
    )
}
