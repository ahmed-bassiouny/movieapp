package com.bassiouny.data.entities

import com.google.gson.annotations.SerializedName

/**
 * Created by Bassiouny
 */
data class MovieData(
    @SerializedName("id") val id: Int,
    @SerializedName("vote_average") val voteAverage: String?,
    @SerializedName("poster_path") val image: String?,
    @SerializedName("title") val title: String?,
    @SerializedName("release_date") val releaseDate: String?,
    @SerializedName("original_language") val originalLanguage: String?,

)

data class MovieList(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: List<MovieData>,

)
