package com.bassiouny.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author by Bassiouny
 */
@Entity(tableName = "favorite_movies")
data class FavoriteMovieDbData(
    @PrimaryKey val movieId: Int
)
