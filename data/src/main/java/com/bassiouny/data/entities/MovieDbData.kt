package com.bassiouny.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Bassiouny
 */
@Entity(tableName = "movies")
data class MovieDbData(
    @PrimaryKey val id: Int,
    val releaseDate: String,
    val image: String,
    val title: String,
    val rating:String,
    val originalLanguage:String
)
