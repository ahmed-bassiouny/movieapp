package com.bassiouny.data.api

import com.bassiouny.data.entities.MovieData
import com.bassiouny.data.entities.MovieList
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Bassiouny
 */
interface MovieApi {
    @GET("movie")
    suspend fun getMovies(
        @Query("primary_release_year") primaryReleaseYear: String,
        @Query("sort_by") sortBy: String,
    ): MovieList

    @GET("/movies")
    suspend fun search(@Query("q") query: String): List<MovieData>
}
