package com.bassiouny.clean.mapper

import com.bassiouny.clean.entities.MovieListItem
import com.bassiouny.clean.util.ImageSize
import com.bassiouny.domain.entities.MovieEntity

/**
 * @author by Bassiouny on 26/08/2022
 */
object MovieEntityMapper {

    fun toPresentation(movieEntity: MovieEntity) = MovieListItem.Movie(
        id = movieEntity.id,
        imageUrl = getFullImagePath(movieEntity.image),
        title = movieEntity.title,
        date = movieEntity.releaseDate,
        rating = getTotalRate(movieEntity.rating),
    )

    private fun getFullImagePath(it:String):String{
       return "https://image.tmdb.org/t/p/${getImageSize()}${it}"
    }

    private fun getImageSize() = ImageSize.W_185.value

    private fun getTotalRate(value:String) = "Vote Average : $value"

}
