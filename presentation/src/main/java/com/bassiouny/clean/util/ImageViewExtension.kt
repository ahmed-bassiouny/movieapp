package com.bassiouny.clean.util

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import com.bassiouny.clean.R
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy

fun AppCompatImageView.loadImage(url: String) = Glide.with(this)
    .asDrawable()
    .format(DecodeFormat.PREFER_RGB_565)
    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
    .thumbnail(getThumbnailRequest(url))
    .load(url)
    .placeholder(R.color.light_gray)
    .error(R.drawable.bg_image)
    .into(this)

private fun AppCompatImageView.getThumbnailRequest(url: String): RequestBuilder<Drawable> = Glide.with(this)
    .asDrawable()
    .format(DecodeFormat.PREFER_RGB_565)
    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
    .sizeMultiplier(0.2F)
    .load(url)

fun AppCompatImageView.clearImage() = Glide.with(this).clear(this)
