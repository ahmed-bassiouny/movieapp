package com.bassiouny.clean.util

enum class ImageSize(val value:String) {
    W_92("w92"),W_185("w185"),W_500("w500"),W_780("w780")
}
