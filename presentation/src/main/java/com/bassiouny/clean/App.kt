package com.bassiouny.clean

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Bassiouny on 13/05/2020
 */
@HiltAndroidApp
class App : Application()
