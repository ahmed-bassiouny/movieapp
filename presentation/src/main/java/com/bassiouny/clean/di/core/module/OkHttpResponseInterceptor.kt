package com.bassiouny.clean.di.core.module

import com.bassiouny.data.BuildConfig
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject


class OkHttpResponseInterceptor @Inject constructor(
): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val url = decorateWithHeaders(request.url().newBuilder()).build()

        val response = chain.proceed(request.newBuilder().url(url).build())

        return response.newBuilder()
            .build()
    }

    private fun decorateWithHeaders(builder: HttpUrl.Builder): HttpUrl.Builder = builder.apply {
        addQueryParameter("api_key",BuildConfig.API_KEY)
    }
}
