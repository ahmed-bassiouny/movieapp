package com.bassiouny.clean.ui.feed.viewholder

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bassiouny.clean.R
import com.bassiouny.clean.databinding.ItemMovieBinding
import com.bassiouny.clean.entities.MovieListItem
import com.bassiouny.clean.util.clearImage
import com.bassiouny.clean.util.loadImage
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy

/**
 * Created by Bassiouny on 13/05/2020
 */
class MovieViewHolder(
    parent: ViewGroup,
    private val onMovieClick: (movieId: Int) -> Unit,
) : RecyclerView.ViewHolder(
    ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false).root
) {

    private val binding = ItemMovieBinding.bind(itemView)

    fun bind(movie: MovieListItem.Movie) = with(binding) {
        image.loadImage(movie.imageUrl)
        tvTitle.text = movie.title
        tvDate.text = movie.date
        tvRate.text = movie.rating
        root.setOnClickListener { onMovieClick(movie.id) }
    }

    fun unBind() = with(binding) {
        image.clearImage()
        root.setOnClickListener(null)
    }


}
