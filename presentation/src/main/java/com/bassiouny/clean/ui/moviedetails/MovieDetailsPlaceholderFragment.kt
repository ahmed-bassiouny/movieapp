package com.bassiouny.clean.ui.moviedetails

import androidx.fragment.app.Fragment
import com.bassiouny.clean.R

/**
 * @author by Bassiouny on 23/08/2022
 */
class MovieDetailsPlaceholderFragment : Fragment(R.layout.fragment_movie_details_placeholder) 
