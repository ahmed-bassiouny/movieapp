package com.bassiouny.clean.entities

/**
 * @author by Bassiouny on 26/08/2022
 */
sealed class MovieListItem {
    data class Movie(
        val id: Int,
        val imageUrl: String,
        val title:String,
        val date:String,
        val rating:String,
    ) : MovieListItem()

    data class Separator(val category: String) : MovieListItem()
}
